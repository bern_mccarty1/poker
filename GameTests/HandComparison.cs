﻿using System;
using System.IO;
using Xunit;
using Poker;

namespace GameTests
{
    public class HandComparison
    {
        [Fact]
        public void AceHighBeatsKingHigh()
        {
            var game = PokerGame.Parse("Cowboy: 2S 3S 4S 9S KH BulletBoy: 2C 3C 4C 9C AD");
            Assert.Equal("BulletBoy", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.HighCard.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
        }

        [Fact]
        public void PairBeatsKingHigh()
        {
            var game = PokerGame.Parse("Cowboy: 2S 3S 4S 9S KH DuecesDude: 2C 2D 4C 9C AD");
            Assert.Equal("DuecesDude", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
        }

        [Fact]
        public void PairBeatsBetterPair()
        {
            var game = PokerGame.Parse("ThreesDude: 3C 3S 4S 9S KH DuecesDude: 2C 2D 4C 9C AD");
            Assert.Equal("ThreesDude", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.King.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());

            Assert.Equal("DuecesDude", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
        }

        [Fact]
        public void LikePairBeatsOnFinalKicker()
        {
            var game = PokerGame.Parse("FourKicker: 3C 3D 4S 9S AH FiveKicker: 3H 3S 5C 9C AD");
            Assert.Equal("FiveKicker", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Five.ToString(), game.HandsBestFirst[0].Cards[2].Rank.ToString());

            Assert.Equal("FourKicker", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Four.ToString(), game.HandsBestFirst[1].Cards[2].Rank.ToString());
        }

        [Fact]
        public void LikePairAndLikeKickersTie()
        {
            var game = PokerGame.Parse("FourKicker1: 3C 3D 4S 9S AH FourKicker2: 3H 3S 4C 9C AD");

            Assert.Equal(0, HandComparer.Instance.Compare(game.HandsBestFirst[0], game.HandsBestFirst[1]));

            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Four.ToString(), game.HandsBestFirst[0].Cards[2].Rank.ToString());

            Assert.Equal(HandRank.Pair.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Four.ToString(), game.HandsBestFirst[1].Cards[2].Rank.ToString());
        }


        [Fact]
        public void TwoPairBeatsPair()
        {
            var game = PokerGame.Parse("TwoPairCowboy: 2S 3S 2D 3D KH DuecesDude: 2C 2H 4C 9C AD");
            Assert.Equal("TwoPairCowboy", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.TwoPairs.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.King.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
        }

        [Fact]
        public void TwoPairBeatsBetterTwoPair()
        {
            var game = PokerGame.Parse("TwoPairCowboy: 2S 3S 2D 3D KH TwoPairBetter: 2C 2H 4C 4D KC");
            Assert.Equal("TwoPairBetter", game.HandsBestFirst[0].Player);
        }

        [Fact]
        public void LikeTwoPairsAndLikeKickersTie()
        {
            var game = PokerGame.Parse("TwoPairsTom: 2S 3S 2D 3D KH TwoPairsDave: 2C 2H 3C 3H KC");
            Assert.Equal(0, HandComparer.Instance.Compare(game.HandsBestFirst[0], game.HandsBestFirst[1]));
        }


        [Fact]
        public void TripsBeatsTwoPair()
        {
            var game = PokerGame.Parse("Cowboy: 2S 3S 2D 3D KH DuecesDude: 2C 2H 4C 9C AD SetMan: 5C JS 5D TS 5H");
            Assert.Equal("SetMan", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.ThreeOfAKind.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Jack.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
        }

        [Fact]
        public void StraightBeatsTrips()
        {
            var game = PokerGame.Parse("NinesCowboy: 9S TS 9D TH KH NinesBullet: 9C 9H TC 8C AH SetMan: 7C JS 7D QS 7H StraightMeister: TD JD QD KD AS");
            Assert.Equal("StraightMeister", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[0].Cards[0].Suit.ToString());
        }

        [Fact]
        public void AceHighStraightBeatsWheelStraight()
        {
            var game = PokerGame.Parse("NinesCowboy: 9S TS 9D TH KH NinesBullet: 9C 9H TC 8C AH SetMan: 7C JS 7D QS 7H WheelStraight: AD 2C 3C 5C 4C AceStraight: TD JD QD KD AS");
            Assert.Equal("AceStraight", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[0].Cards[0].Suit.ToString());

            Assert.Equal("WheelStraight", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[1].Rank.ToString());
        }

        [Fact]
        public void WheelStraightTiesWheelStraight()
        {
            var game = PokerGame.Parse("NinesCowboy: 9S TS 9D TH KH NinesBullet: 9C 9H TC 8C AH SetMan: 7C JS 7D QS 7H WheelStraight1: AD 2C 3C 5C 4C WheelStraight2: AC 2D 3D 5D 4D");
            Assert.Equal(0, HandComparer.Instance.Compare(game.HandsBestFirst[0], game.HandsBestFirst[1]));

            Assert.StartsWith("WheelStraight", game.HandsBestFirst[0].Player);
            Assert.StartsWith("WheelStraight", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[1].Rank.ToString());
        }

        [Fact]
        public void FlushBeatsStraight()
        {
            var game = PokerGame.Parse("Cowboy: 2S 3S 2D 3D KH MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H StraightMeister: TD JD QD KD AS");
            Assert.Equal("MrFlush", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[0].Cards[0].Suit.ToString());

            // The next best should be the Ace high straight
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[1].Cards[0].Suit.ToString());
        }

        [Fact]
        public void FlushBeatsFlush()
        {
            var game = PokerGame.Parse("LosingFlush: 2C 4C 5C 6C AC SetMan: 9C JS 9D TS 9H WinningFlush: 3H 4H 5H 6H AH");
            Assert.Equal("WinningFlush", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Hearts.ToString(), game.HandsBestFirst[0].Cards[0].Suit.ToString());

            Assert.Equal("LosingFlush", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[1].Cards[0].Suit.ToString());
        }


        [Fact]
        public void FullHouseBeatsFlush()
        {
            var game = PokerGame.Parse("CaptainBoat: 2S 3S 2D 3D 3H MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H StraightMeister: TD JD QD KD AS");
            Assert.Equal("CaptainBoat", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Three.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[0].Cards[3].Rank.ToString());

            // The next best is the Flush
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[1].Cards[0].Suit.ToString());

            // The next best should be the Ace high straight
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[2].Cards[0].Suit.ToString());
        }

        [Fact]
        public void FullHouseBeatsFullHouse()
        {
            var game = PokerGame.Parse("LosingBoat: 2S 3S 2D 3D 3H MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H StraightMeister: TD JD QD KD AS WinningBoat: 6C KC 6D KS 6S");
            Assert.Equal("WinningBoat", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.King.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Six.ToString(), game.HandsBestFirst[0].Cards[3].Rank.ToString());

            Assert.Equal("LosingBoat", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Three.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[1].Cards[3].Rank.ToString());

            // The next best is the Flush
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[2].Cards[0].Suit.ToString());

            // The next best should be the Ace high straight
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[3].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[3].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[3].Cards[0].Suit.ToString());
        }


        [Fact]
        public void FourOfAKindBeatsFullHouse()
        {
            var game = PokerGame.Parse("CaptainBoat: 2S 3S 2D 3D 3H MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H StraightMeister: TD JD QD KD AS MrQuads: 7C 7D 7H 7S 5S");
            Assert.Equal("MrQuads", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.FourOfAKind.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Seven.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());

            // The next best is the FullHouse
            Assert.Equal("CaptainBoat", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Three.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[1].Cards[3].Rank.ToString());

            // The next best is the Flush
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[2].Cards[0].Suit.ToString());

            // The next best should be the Ace high straight
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[3].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[3].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[3].Cards[0].Suit.ToString());
        }

        [Fact]
        public void FourOfAKindBeatsFourOfAKind()
        {
            var game = PokerGame.Parse("CaptainBoat: 2S 3S 2D 3D 3H MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H StraightMeister: TD JD QD KD AS MrSevens: 7C 7D 7H 7S 5S MrEights: 8C 8D 8H 8S 9D");

            Assert.True(0 > HandComparer.Instance.Compare(game.Hands[4], game.Hands[5]), "Sevens are not greater than Eights");

            Assert.Equal("MrEights", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.FourOfAKind.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Eight.ToString(), game.HandsBestFirst[0].Cards[1].Rank.ToString());

            Assert.Equal("MrSevens", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.FourOfAKind.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Seven.ToString(), game.HandsBestFirst[1].Cards[1].Rank.ToString());

            // The next best is the FullHouse
            Assert.Equal("CaptainBoat", game.HandsBestFirst[2].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Three.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[2].Cards[3].Rank.ToString());

            // The next best is the Flush
            Assert.Equal(HandRank.Flush.ToString(), game.HandsBestFirst[3].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[3].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Clubs.ToString(), game.HandsBestFirst[3].Cards[0].Suit.ToString());

            // The next best should be the Ace high straight
            Assert.Equal(HandRank.Straight.ToString(), game.HandsBestFirst[4].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[4].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardSuit.Spades.ToString(), game.HandsBestFirst[4].Cards[0].Suit.ToString());
        }


        [Fact]
        public void StraightFlushBeatsFourOfAKind()
        {
            var game = PokerGame.Parse("CaptainBoat: 2S 3S 2D 3D 3H MrFlush: 2C 3C 4C 9C AC SetMan: 5C JS 5D TS 5H SFMeister: TD JD QD KD AD MrQuads: 7C 7D 7H 7S 5S");

            Assert.Equal("SFMeister", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());

            Assert.Equal("MrQuads", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.FourOfAKind.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Seven.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());

            // The next best is the FullHouse
            Assert.Equal("CaptainBoat", game.HandsBestFirst[2].Player);
            Assert.Equal(HandRank.FullHouse.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Three.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[2].Cards[3].Rank.ToString());
        }

        [Fact]
        public void StraightFlushBeatsStraightFlush()
        {
            var game = PokerGame.Parse("SetMan: 5D JS 5S TS 5H SFAce: TD JD QD KD AD SFWheel: 2C 3C 5C 4C AC");

            Assert.Equal("SFAce", game.HandsBestFirst[0].Player);
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());

            Assert.Equal("SFWheel", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[1].Cards[4].Rank.ToString());
        }

        [Fact]
        public void StraightFlushTiesStraightFlush()
        {
            var game = PokerGame.Parse("SetMan: 7D JS 7S TS 7H SFAceOne: TD JD QD KD AD SFWheel: 2C 3C 5C 4C AC SFAceTwo: TH JH QH KH AH");

            Assert.Equal(0, HandComparer.Instance.Compare(game.HandsBestFirst[0], game.HandsBestFirst[1]));
            Assert.True(0 < HandComparer.Instance.Compare(game.HandsBestFirst[0], game.HandsBestFirst[2]));

            Assert.StartsWith("SFAce", game.HandsBestFirst[0].Player);
            Assert.StartsWith("SFAce", game.HandsBestFirst[1].Player);
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[0].Rank.ToString());
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[1].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[0].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[1].Cards[0].Rank.ToString());

            Assert.Equal("SFWheel", game.HandsBestFirst[2].Player);
            Assert.Equal(HandRank.StraightFlush.ToString(), game.HandsBestFirst[2].Rank.ToString());
            Assert.Equal(Poker.CardRank.Ace.ToString(), game.HandsBestFirst[2].Cards[0].Rank.ToString());
            Assert.Equal(Poker.CardRank.Deuce.ToString(), game.HandsBestFirst[2].Cards[4].Rank.ToString());
        }
    }
}
