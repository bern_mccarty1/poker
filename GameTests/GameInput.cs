using System;
using System.IO;
using Xunit;
using Poker;

namespace GameTests
{
    public class GameInput
    {
        [Fact]
        public void OnePlayerGameThrows()
        {
            Assert.Throws<InvalidDataException>(() => PokerGame.Parse("Loser1: 2C 3C 4C 5C 7D"));
        }

        [Fact]
        public void TooManyPlayersThrows()
        {
            Assert.Throws<InvalidDataException>(() => PokerGame.Parse("Loser1: 2C 3C 4C 5C 6C Lucky1: 7C 8C 9C TC JC Lucky2: QC KC AC 2D 3D Lucky3: 4D 5D 6D 7D 8D BoatBoy: 9D TD JD QD KD AD  FlushMan: 2H 3H 4H 5H 6H CowboyHigh: 7H 8H 9H TH JH  OneBullet: QH KH AH 2S 3S OneTooMany: 4S 5S 6S 7S 8S"));
        }

        [Fact]
        public void MaxPlayersGameParses()
        {
            var game = PokerGame.Parse("Loser1: 2C 3C 4C 5C 6C Lucky1: 7C 8C 9C TC JC Lucky2: QC KC AC 2D 3D Lucky3: 4D 5D 6D 7D 8D BoatBoy: 9D TD JD QD KD AD  FlushMan: 2H 3H 4H 5H 6H CowboyHigh: 7H 8H 9H TH JH  OneBullet: QH KH AH 2S 3S");
            Assert.NotNull(game);
            Assert.Equal(8, game.Hands.Length);
        }

        [Fact]
        public void NoHandMatchesInGameThrows()
        {
            Assert.Throws<InvalidDataException>(() => PokerGame.Parse("Loser1: x2C 3C 4C 5C 7D Loser2: x2D 3D 4D 5D 7S Lucky1: xTC JC QC KC AC Lucky2: TD JD QD KD AD Lucky3: xTH JH QH KH AH BoatBoy: x2H 4S 4C 2D 4H  FlushMan: x2S 8S AS QS 3S CowboyHigh: x2H 3D 5S 9C KD  OneBullet: x2C 3H 4S 8C AH "));
        }

        [Fact]
        public void DuplicateCardsInGameThrows()
        {
            Assert.Throws<InvalidDataException>(() => PokerGame.Parse("Loser1: 2C 3C 4C 5C 7D Lucky1: TC JC QC KC AC Lucky2: TD JD QD KD AD Lucky3: TH JH QH KH AH BoatBoy: 2H 4S 4C 2D 4H  FlushMan: 2S 8S AS QS 3S CowboyHigh: 2H 3D 5S 9C KD  OneBullet: 2C 3H 4S 8C AH "));
        }
    }
}
