
# Poker Hands

All commands shown will be from the folder above all of the project folders: Poker, PokerCore, et cetera.

 This stuff was developed with .NET 5. To build it try:

	dotnet build .\Poker\Poker.sln

To run the xUnit tests try:

	dotnet test .\Poker\Poker.sln

The executable project is "Poker". If you run it with no arguments you will see:

	.\Poker\bin\Debug\net5.0\Poker
	You must provide a single argument which must be either a filename or - to read from stdin.

There are some test poker games in the file .\Poker\TestGames1.txt. Here we supply the filename as the argument:

	.\Poker\bin\Debug\net5.0\Poker .\Poker\TestGames1.txt

You can evaluate the same gamefile by redirecting the file to the program while supplying "-" as the program argument.

On PowerShell Core:

	Get-Content .\Poker\TestGames1.txt | .\Poker\bin\Debug\net5.0\Poker.exe -

On cmd shell on Windows or on bash on Linux probably:

	.\Poker\bin\Debug\net5.0\Poker - < .\Poker\TestGames1.txt

You may also input lines on the keyboard one game per line and they will be evaluated as you go. To exit simply type enter (a blank line) rather than inputing another game:

	PS D:\temp\Poker> .\Poker\bin\Debug\net5.0\Poker -
	Enter poker games one game per-line or, to quit, just press enter.
	Black: 2H 4S 4C 2D 4H  White: 2S 8S AS QS 3S
	Black wins. - with FullHouse: Four over Deuce
	9sFullBeats4sFull: 3D 9C 9D 3C 9H Loser: 2C 4C 4H 4D 2S 4S
	9sFullBeats4sFull wins. - with FullHouse: Nine over Three
	TieStraight: AC 2C 3S 5D 4H OtherGuy: 5H 4D AH 2H 3C
	Tie between (OtherGuy,TieStraight) with 2 Straight hands
	