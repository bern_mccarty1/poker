﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Poker
{
    public enum HandRank
    {
        HighCard,
        Pair,
        TwoPairs,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraightFlush
    }


    public enum CardRank
    {
        Deuce,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }

    public enum CardSuit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }

    public record Card(CardRank Rank, CardSuit Suit);

    public class CardComparerRankOnly : IComparer<Card>
    {
        private static CardComparerRankOnly? s_Instance = null;
        public static CardComparerRankOnly Instance => s_Instance ??= new CardComparerRankOnly();

        public int Compare(Card a, Card b)
        {
            if (a.Rank == b.Rank)
                return 0;
            if (a.Rank < b.Rank)
                return -1;
            return 1;
        }
    }

    // This class represents rank sets such as pairs, trips, quads
    public record RankSet(int Count, CardRank Rank);

    class SetsRagsSuits
    {
        public readonly List<RankSet> Sets = new(2);    // There are at most 2 rank sets in a five card poker hand
        public readonly List<Card> Rags = new(5);       // These are cards not part of any set. They say nothing about if you have a straight or flush or not.
        public readonly List<CardSuit> Suits = new(4);  // Simply the distinct set of suits that are in the hand.
    }

    public class Hand
    {
        public Hand(string player, Card[] cards)
        {
            Player = player;

            // We place our Cards in descending order by rank (the suit does not influence our Cards order here).
            Cards = new Card[5];
            Array.Copy(cards, Cards, Cards.Length);
            Array.Sort(Cards, CardComparerRankOnly.Instance);
            Array.Reverse(Cards);
        }
        public string Player { get; set; }
        public Card[] Cards { get; }

        private HandRank? _handRank;
        public HandRank Rank
        {
            get => _handRank ??= GetHandRank();
        }

        private Card[]? _cardsInTieBreakOrder;
        internal Card[] CardsInTieBreakOrder
        {
            get => _cardsInTieBreakOrder ??= GetCardsInTieBreakOrder();
        }

        private SetsRagsSuits? _setsRagsSuits;
        private SetsRagsSuits SetsRagsSuits
        {
            get => _setsRagsSuits ??= GetSetsRagsSuits();
        }

        private HandRank GetHandRank()
        {
            var sets = SetsRagsSuits.Sets;

            bool flush = SetsRagsSuits.Suits.Count == 1;
            bool straight = sets.Count == 0 && IsStraight();

            if (straight && flush)
                return HandRank.StraightFlush;
            if (sets.Count == 1 && sets[0].Count == 4)
                return HandRank.FourOfAKind;
            if (sets.Count == 2 && sets[0].Count == 3)
                return HandRank.FullHouse;
            if (flush)
                return HandRank.Flush;
            if (straight)
                return HandRank.Straight;
            if (sets.Count == 1 && sets[0].Count == 3)
                return HandRank.ThreeOfAKind;
            if (sets.Count == 2)
                return HandRank.TwoPairs;
            if (sets.Count == 1)
                return HandRank.Pair;
            return HandRank.HighCard;
        }

        public string GetHandDescription()
        {
            string description = this.Rank switch
            {
                HandRank.HighCard or HandRank.Straight or HandRank.Flush or HandRank.StraightFlush => CardsInTieBreakOrder[0].Rank.ToString(),
                HandRank.Pair or HandRank.ThreeOfAKind or HandRank.FourOfAKind => CardsInTieBreakOrder[0].Rank.ToString(),
                HandRank.TwoPairs or HandRank.FullHouse => $"{SetsRagsSuits.Sets[0].Rank} over {SetsRagsSuits.Sets[1].Rank}",
                _ => throw new ArgumentOutOfRangeException($"{nameof(HandRank)}: {(int)this.Rank}"),
            };
            return description;
        }

        private bool IsStraight()
        {
            // To make detecting the wheel straight case easier we first see if our lowest 4 ranking cards are a run.

            // If the lowest 4 cards aren't a run, we don't have a 5 card straight.
            if (!(Cards[4].Rank == Cards[3].Rank - 1 && Cards[3].Rank == Cards[2].Rank - 1 && Cards[2].Rank == Cards[1].Rank - 1))
                return false;

            return Cards[0].Rank == Cards[1].Rank + 1 || (Cards[0].Rank == CardRank.Ace && Cards[4].Rank == CardRank.Deuce);
        }

        private SetsRagsSuits GetSetsRagsSuits()
        {
            var srs = new SetsRagsSuits();

            // We know that Cards is always sorted highest rank to lowest rank
            // Construct sets of cards with like rank (pairs, trips and quads) as we loop through the sorted cards
            // Cards not part of any same-rank-set go in Rags.
            var setStart = -1;
            for (var cndx = 0; cndx < Cards.Length; ++cndx)
            {
                var card = Cards[cndx];
                if (!srs.Suits.Contains(card.Suit))
                    srs.Suits.Add(card.Suit);

                if (cndx > 0)
                {
                    var prevCard = Cards[cndx - 1];

                    // If not in a set already and the recent 2 cards have same rank, record the start of a set
                    if (setStart == -1)
                    {
                        if (prevCard.Rank == card.Rank)
                            setStart = cndx - 1;
                    }

                    // If we've a current set and our 2 adjacent cards do not have the same rank then end and record the set.
                    if (setStart != -1)
                    {
                        if (prevCard.Rank != card.Rank)
                        {
                            srs.Sets.Add(new RankSet(Count: cndx - setStart, Rank: prevCard.Rank));
                            setStart = -1;
                        }
                    }
                    // if we've no current set and our 2 adjacent cards do not match on rank, then record prevCard as a rag
                    else
                    {
                        if (prevCard.Rank != card.Rank)
                            srs.Rags.Add(prevCard);
                    }

                    // Deal with the final card
                    if (cndx == 4)
                    {
                        if (prevCard.Rank != card.Rank)
                            srs.Rags.Add(card);
                        else
                        {
                            if (setStart == -1)
                                setStart = cndx - 1;
                            srs.Sets.Add(new RankSet(Count: cndx - setStart + 1, Rank: card.Rank));
                        }
                    }
                }
            }

            // Sort our rank sets from the largest sets to smallest sets: quads, trips, pairs
            srs.Sets.Sort((seta, setb) => setb.Count.CompareTo(seta.Count));

            return srs;
        }

        private IEnumerable<Card> GetSetsAndRagsOrderedCards()
        {
            if (Rank is not (HandRank.Pair or HandRank.TwoPairs or HandRank.ThreeOfAKind or HandRank.FullHouse or HandRank.FourOfAKind))
                throw new InvalidOperationException($"Hand.Rank value {Rank} is not valid for calling method GetSetsAndRagsOrderedCards()");

            var orderedCards = new List<Card>(5);

            // We already know that SetsRagsSuits.Sets is ordered from larger sets to smaller sets.
            foreach (var set in this.SetsRagsSuits.Sets)
                orderedCards.AddRange(Enumerable.Repeat(new Card(set.Rank, CardSuit.Clubs), set.Count));

            orderedCards.AddRange(this.SetsRagsSuits.Rags);

            return orderedCards;
        }

        private IEnumerable<Card> GetStraightOrderedCards()
        {
            List<Card> tbo = new List<Card>(5);
            tbo.AddRange(Cards);

            // If the straight is a wheel straight, swap the Ace from first card to last: 5,4,3,2,A
            if (tbo[0].Rank == CardRank.Ace && tbo[4].Rank == CardRank.Deuce)
            {
                var tmp = tbo[0];
                tbo[0] = tbo[4];
                tbo[4] = tmp;
            }

            return tbo;
        }

        private Card[] GetCardsInTieBreakOrder()
        {
            IEnumerable<Card> tbo = this.Rank switch
            {
                HandRank.HighCard or HandRank.Flush => Cards,
                HandRank.Pair or HandRank.TwoPairs or HandRank.ThreeOfAKind or HandRank.FullHouse or HandRank.FourOfAKind => GetSetsAndRagsOrderedCards(),
                HandRank.Straight or HandRank.StraightFlush => GetStraightOrderedCards(),
                _ => throw new ArgumentOutOfRangeException($"{nameof(HandRank)}: {(int)this.Rank}"),
            };

            return tbo.ToArray();
        }

    }

    public class HandComparer : IComparer<Hand>
    {
        private static HandComparer? s_Instance = null;
        public static HandComparer Instance => s_Instance ??= new HandComparer();

        public int Compare(Hand? a, Hand? b)
        {
#pragma warning disable CS8602 // Dereference of a possibly null reference.
            if (a.Rank < b.Rank)
#pragma warning restore CS8602 // Dereference of a possibly null reference.
                return -1;
            if (a.Rank > b.Rank)
                return 1;

            // We have a tie in the HandRank of these two hands.
            // We will attempt to break the tie via our CardsInTieBreakOrder property. The precise order within CardsInTieBreakOrder is HandRank specific in a way
            // that allows us to simply compare cards by rank in the same position from start to end to determine the greater hand (or it can still be a tie if they are all the same rank).
            for (var cardNdx = 0; cardNdx < a.CardsInTieBreakOrder.Length; ++cardNdx)
            {
                if (a.CardsInTieBreakOrder[cardNdx].Rank < b.CardsInTieBreakOrder[cardNdx].Rank)
                    return -1;
                if (a.CardsInTieBreakOrder[cardNdx].Rank > b.CardsInTieBreakOrder[cardNdx].Rank)
                    return 1;
            }
            return 0;
        }
    }

}