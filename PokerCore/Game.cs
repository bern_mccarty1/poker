﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Poker
{
    public class PokerGame
    {
        public static PokerGame Parse(string gameString)
        {
            var matches = Regex.Matches(gameString, @"(?:^|\s+)(?<player>\w{2,32}):(?: (?<rank>[2-9TJQKA])(?<suit>[CDHS])){5}");

            if (matches.Count == 0)
                throw new InvalidDataException($"Game contains invalid poker hand syntax: {gameString}.");
            if (matches.Count == 1)
                throw new InvalidDataException($"Game contains only one player: {gameString}.");
            if (matches.Count > 8)
                throw new InvalidDataException($"Game contains too many players: {gameString}.");

            var game = new PokerGame(new Hand[matches.Count]);
            for (int matchNdx = 0; matchNdx < matches.Count; ++matchNdx)
            {
                var match = matches[matchNdx];
                var cards = new Card[5];
                for (var cardNdx = 0; cardNdx < 5; ++cardNdx)
                {
                    CardRank rank = (CardRank)"23456789TJQKA".IndexOf(match.Groups["rank"].Captures[cardNdx].Value[0]);
                    CardSuit suit = (CardSuit)"CDHS".IndexOf(match.Groups["suit"].Captures[cardNdx].Value[0]);
                    cards[cardNdx] = new Card(rank, suit);
                }

                game.Hands[matchNdx] = new Hand(match.Groups["player"].Value, cards);
            }

            // Detect duplicate cards within a game as bad data.
            var allDeltCards = new List<Card>(40);
            foreach (var hand in game.Hands)
                allDeltCards.AddRange(hand.Cards);
            if (allDeltCards.Distinct().Count() != allDeltCards.Count())
                throw new InvalidDataException($"Game contains duplicate cards:  on line {gameString}.");

            return game;
        }

        public static IEnumerable<PokerGame> Games(IEnumerable<string> gameStrings)
        {
            foreach (var gameString in gameStrings)
            {
                // Ignore blank lines in the file
                if (string.IsNullOrWhiteSpace(gameString))
                    continue;
                yield return PokerGame.Parse(gameString);
            }
        }

        public static IEnumerable<PokerGame> Games(string filename)
        {
            return Games(File.ReadLines(filename, Encoding.UTF8));
        }

        public Hand[] Hands;
        private PokerGame(Hand[] hands)
        {
            Hands = hands;
        }

        private Hand[]? _handsBestFirst;
        public Hand[] HandsBestFirst
        {
            get
            {
                if (null == _handsBestFirst)
                {
                    _handsBestFirst = new Hand[Hands.Length];
                    Array.Copy(Hands, _handsBestFirst, Hands.Length);
                    Array.Sort(_handsBestFirst, HandComparer.Instance);
                    Array.Reverse(_handsBestFirst);
                }
                return _handsBestFirst;
            }
        }

        public string GetOutcomeDescription()
        {
            var winnerNames = new List<string>() { HandsBestFirst[0].Player };
            for (int handNdx = 1; handNdx < HandsBestFirst.Length; ++handNdx)
            {
                if (0 == HandComparer.Instance.Compare(HandsBestFirst[0], HandsBestFirst[handNdx]))
                    winnerNames.Add(HandsBestFirst[handNdx].Player);
                else
                    break;
            }
            if (winnerNames.Count > 1)
                return $"Tie between ({string.Join(',', winnerNames)}) with {winnerNames.Count} {HandsBestFirst[0].Rank} hands";

            return $"{HandsBestFirst[0].Player} wins. - with {HandsBestFirst[0].Rank}: {HandsBestFirst[0].GetHandDescription()}";
        }

    }
}
