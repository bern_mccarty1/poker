﻿using System;
using System.Collections.Generic;

namespace Poker
{
    internal class Program
    {
        static int Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine($"You must provide a single argument which must be either a filename or - to read from stdin.");
                return 1;
            }
            else
            {
                IEnumerable<PokerGame> games;
                if (args[0].Equals("-"))
                {
                    if (!Console.IsInputRedirected)
                        Console.WriteLine("Enter poker games one game per-line or, to quit, just press enter.");
                    while (true)
                    {
                        var line = Console.ReadLine();
                        if (line == null || (!Console.IsInputRedirected && string.IsNullOrWhiteSpace(line)))
                            break;
                        // If we're taking input from a redirected file, then we'll ignore blank lines (rather than quit)
                        if (Console.IsInputRedirected && string.IsNullOrWhiteSpace(line))
                            continue;
                        var game = PokerGame.Parse(line);
                        Console.WriteLine(game.GetOutcomeDescription());
                    }
                }
                else
                {
                    games = PokerGame.Games(args[0]);
                    foreach (var game in games)
                        Console.WriteLine(game.GetOutcomeDescription());
                }

                return 0;
            }
        }
    }
}
